/**
 * 
 */
package huristic;
/**
*@author tonychao 
*创建时间 ：2018年4月20日 上午11:03:56
*功能  返回在standlone 模式下可以利用的资源
*/

import java.util.ArrayList;
import others.Global;

public class yarnResourceEstimator extends Estimator{

	//初始化时对estimater 进行赋值
	public yarnResourceEstimator(int executor_cores,int executor_memory,int cores_per_task){		
		super(executor_cores, executor_memory, cores_per_task);
	}
	
	//对estimator 拿到的数据进行估算,
	@Override
	public void conf(int executor_cores,int executor_memory,int cores_per_task){
		this.availableCores=this.avaliableMemory=this.avaliableExecutors=this.avalibaleParallelism=0;
		this.executor_cores=executor_cores; this.executor_memory=executor_memory; this.cores_per_task=cores_per_task;
		//yarn模式下这个节点累加即可
		for (int i=0;i<memorys.size();i++){
			if (memorys.get(i)>=executor_memory&&cores.get(i)>=executor_cores) {
				this.avaliableExecutors+=1;	
				this.availableCores+=Math.min(executor_cores, cores.get(i));
				this.avalibaleParallelism+=(int) (Math.floor(Math.min(executor_cores, cores.get(i))/cores_per_task));
				//System.out.println((int) (Math.floor(Math.min(executor_cores, cores.get(i))/cores_per_task)));
			}
		}
		this.avaliableMemory=this.avaliableExecutors*executor_memory;
	}
	
}
