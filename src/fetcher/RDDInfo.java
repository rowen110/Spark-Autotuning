package fetcher;
/**
*@author tonychao 
*创建时间 ：2017年6月12日
*功能 记录一个RDD的信息，但是这些信息只有日志文件才能获得
*/
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class RDDInfo {
	public
	String name,scope,callSite;
	int ID=-1;
	ArrayList<Integer> parientIDList =null;
	int replication =-1; 
	int numberOfPartitions =-1;
	int numberOfCachedPartitions=-1;
	int memorySize=-1;
	int diskSize=-1;
	boolean useDisk,useMemory,Deserialized;
	
	/*
	 *源Json： "SparkListenerJobStart"--"Stage Infos"--"RDD Info"--一个对象
	 * 
	 * */
	 public RDDInfo(JSONObject dataJson) throws JSONException{
		 ID = dataJson.getInt("RDD ID");
		 name=dataJson.getString("Name");
		 scope=dataJson.getString("Scope");
		 callSite=dataJson.getString("Callsite");
		 parientIDList=new ArrayList<Integer>();
		 JSONArray jsonParientList= dataJson.getJSONArray("Parent IDs");
		 for (int i=0;i<jsonParientList.length();i++){
			 //System.err.println(jsonParientList.get(i));
			 parientIDList.add(jsonParientList.getInt(i));
		 }
		 //Storage level
		 JSONObject tmpjsonStorageLevel=dataJson.getJSONObject("Storage Level");
		 useDisk=tmpjsonStorageLevel.getBoolean("Use Disk");
		 useMemory=tmpjsonStorageLevel.getBoolean("Use Memory");
		 Deserialized=tmpjsonStorageLevel.getBoolean("Deserialized");
		 replication=tmpjsonStorageLevel.getInt("Replication");
		 
		 //
		 numberOfPartitions=dataJson.getInt("Number of Partitions");
		 numberOfCachedPartitions=dataJson.getInt("Number of Cached Partitions");
		 memorySize=dataJson.getInt("Number of Cached Partitions");
		 memorySize=dataJson.getInt("Memory Size");
		 diskSize=dataJson.getInt("Disk Size");
		 
	 }
	

}
